# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Android application consisting of two screens for displaying
  information about Cat Sky News. The news data is be obtained from
  the json file at the moment and will use api once it is ready

  The first screen have a list of news including headline, webLink and adverts. I displayed all available news
  from the json and once we add api url then it will automatically get data from api. The app is an appropriate UI to indicate to the user that it is busy while
  it fetches results from the API.

  When the user taps on a story news in list the app switches to the second screen showing
  more detail about the story. The second screen shows labels and data values
  for each of the following story attributes:
  • Image
  • Headline
  • Teaser text

  The app is a prototype at the moment so I am using same UI as provided but also have suggestions for more functionality which is not implemented because for that
  we need to change json, following are the suggestions:
  • isLive flag -> it will tell user if it is live or not
  • video feature if available for story
  • likes and dislikes of users

* Version 1.0
* Repository URL: https://shamoon192@bitbucket.org/shamoon192/sky-cat-news.git
* MVVM technical approach/design pattern is followed with data binding




### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact