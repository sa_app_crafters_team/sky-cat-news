package com.shamoon.skycatnews.constants

import com.shamoon.skycatnews.BuildConfig

const val EXTRA_STORY_ID = "${BuildConfig.APPLICATION_ID}_EXTRA_STORY_ID"