package com.shamoon.skycatnews.utils

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat.getSystemService
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shamoon.skycatnews.R
import com.shamoon.skycatnews.repository.model.list.NewsList
import com.shamoon.skycatnews.repository.model.story.Story
import org.ocpsoft.prettytime.PrettyTime
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class Utils {


	fun convertStringToTimeAgo(value: String): String {
		if (value.isNotEmpty()){
			val prettyTime = PrettyTime(Locale.getDefault())
			//val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z", Locale.getDefault())
			val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
			var date = Date()
			try {
				date = formatter.parse(value)
			} catch (ex: Exception) {
				ex.printStackTrace()
			}
			return prettyTime.format(Date(date.time))
		}else{
			return value
		}
	}
	fun isOnline(context: Context): Boolean {
		val connectivityManager =
			context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager?
		val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
		return activeNetworkInfo != null && activeNetworkInfo.isConnected
	}
	fun isOnline(): Boolean {
		val runtime = Runtime.getRuntime()
		try {
			val ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
			val exitValue = ipProcess.waitFor()
			return exitValue == 0
		} catch (e: IOException) {
			e.printStackTrace()
		} catch (e: InterruptedException) {
			e.printStackTrace()
		}
		return false
	}
	//this function can read any file in raw folder in resources and return string text from that file
	private fun getJsonDataFromAsset(context: Context, rawResource: Int): String {
		val jsonString: String
		try {
			jsonString = context.resources.openRawResource(rawResource).bufferedReader().use { it.readText() }
		} catch (ioException: IOException) {
			ioException.printStackTrace()
			return ""
		}
		return jsonString
	}

	fun getNewsListFromFile(context: Context) : NewsList {
		val jsonFileString = getJsonDataFromAsset(context, R.raw.sample_list)

		val listNews = object : TypeToken<NewsList>() {}.type

		return Gson().fromJson(jsonFileString, listNews)
	}

	fun getStoryFromFile(context: Context) : Story {
		val jsonFileString = getJsonDataFromAsset(context, R.raw.sample_story1)

		val story = object : TypeToken<Story>() {}.type

		return Gson().fromJson(jsonFileString, story)
	}
}