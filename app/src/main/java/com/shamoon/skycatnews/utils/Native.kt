package com.shamoon.skycatnews.utils

class Native {
	companion object {
		// Used to load the 'native-lib' library on application startup.
		init {
			System.loadLibrary("native-lib")
		}
	}

	external fun apiURLFromJNI(): String
}