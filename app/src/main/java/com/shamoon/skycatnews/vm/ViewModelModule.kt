package com.shamoon.skycatnews.vm

import android.content.Context
import com.shamoon.skycatnews.repository.model.story.Story
import com.shamoon.skycatnews.ui.activities.story.StoryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
	viewModel { (id: String) -> StoryViewModel(id) }
}