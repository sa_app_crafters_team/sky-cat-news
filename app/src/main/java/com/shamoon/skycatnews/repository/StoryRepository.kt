package com.shamoon.skycatnews.repository

import android.content.Context
import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.shamoon.skycatnews.repository.api.Api
import com.shamoon.skycatnews.repository.model.story.HeroImage
import com.shamoon.skycatnews.repository.model.story.Story
import com.shamoon.skycatnews.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StoryRepository : BaseObservable() {
	val story: MutableLiveData<Story> by lazy {
		MutableLiveData<Story>()
	}

	fun fetchStory(context: Context, id: String) {
		if (Api.isAPIUrlAvailable()) {
			val callback: Callback<Story> = object : Callback<Story> {
				override fun onResponse(call: Call<Story>, response: Response<Story>) {
					if (response.isSuccessful) {
						val body: Story? = response.body()
						story.value = body
						Log.d("news", story.toString())
					}
				}

				override fun onFailure(call: Call<Story>, t: Throwable) {
					Log.e("News List", t.message, t)
				}

			}

			Api.getApi().getStory(id).enqueue(callback)

		} else {
			fetchOfflineStory(id, context)
		}
	}

	fun fetchOfflineStory(id: String, context: Context){
		if (id.equals("1")) {
			story.value = Utils().getStoryFromFile(context)
		}else {
			story.value = Story(listOf(),"","", HeroImage("",""),"","","Story does not exists")
		}
	}
}