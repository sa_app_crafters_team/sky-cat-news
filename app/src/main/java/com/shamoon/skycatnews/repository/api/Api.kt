package com.shamoon.skycatnews.repository.api

import com.shamoon.skycatnews.utils.Native
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
object Api {
	private lateinit var api: ApiInterface
	//we are getting api url from C++ class to keep it secure and we can do the same for api keys and tokens
	private val BASE_URL = ""
	//Native().apiURLFromJNI()

	fun getApi(): ApiInterface {
		val retrofit = Retrofit.Builder()
			.baseUrl(BASE_URL)
			.addConverterFactory(GsonConverterFactory.create())
			.build()
		api = retrofit.create(ApiInterface::class.java)
		return api
	}

	fun isAPIUrlAvailable() : Boolean {
		return BASE_URL.trim().isNotEmpty()
	}
}