package com.shamoon.skycatnews.repository.api

import com.shamoon.skycatnews.repository.model.list.NewsList
import com.shamoon.skycatnews.repository.model.story.Story
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
	@GET("news-list")
	fun getNews(): Call<NewsList>

	@GET("story/{id}")
	fun getStory(@Path("id") id: String): Call<Story>
}