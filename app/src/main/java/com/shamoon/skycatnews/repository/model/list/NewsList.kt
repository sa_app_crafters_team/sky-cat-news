package com.shamoon.skycatnews.repository.model.list

import com.shamoon.skycatnews.utils.Utils
import java.io.Serializable

data class NewsList(
	var `data`: ArrayList<Data>?,
	var title: String?
) : Serializable {
	fun getStory1TimeAgo(): String {
		//get first story teaser text from NewsList
		return Utils().convertStringToTimeAgo(data?.get(0)?.modifiedDate ?: "")
	}

	fun getStory1Headline(): String {
		//get first story headline from NewsList
		return data?.get(0)?.headline ?: "No Headline available"
	}

	fun getStory1TeaserText(): String {
		//get first story teaser text from NewsList
		return data?.get(0)?.teaserText ?: "No teaser text available"
	}

	fun getStory1Image(): String {
		//get first story teaser text from NewsList
		return data?.get(0)?.teaserImage?._links?.url?.href ?: ""
	}

}