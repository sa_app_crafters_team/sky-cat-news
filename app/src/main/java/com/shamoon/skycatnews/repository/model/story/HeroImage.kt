package com.shamoon.skycatnews.repository.model.story

import java.io.Serializable

data class HeroImage(
	val accessibilityText: String,
	val imageUrl: String
) : Serializable