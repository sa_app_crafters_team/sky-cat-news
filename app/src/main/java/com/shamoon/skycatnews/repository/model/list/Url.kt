package com.shamoon.skycatnews.repository.model.list

import java.io.Serializable

data class Url(
	val href: String,
	val templated: Boolean,
	val type: String
) : Serializable