package com.shamoon.skycatnews.repository.model.story

import java.io.Serializable

data class Content(
	val accessibilityText: String,
	val text: String,
	val type: String,
	val url: String
) : Serializable