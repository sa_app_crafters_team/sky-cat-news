package com.shamoon.skycatnews.repository.model.story

import com.shamoon.skycatnews.utils.Utils
import java.io.Serializable

data class Story(
	val contents: List<Content>,
	val creationDate: String,
	val headline: String,
	val heroImage: HeroImage,
	val id: String,
	val modifiedDate: String,
	val message: String = ""
) : Serializable {

	fun isLive(): Boolean {
		return creationDate.isEmpty() && modifiedDate.isEmpty()
	}
	fun getTimeAgo(
	): String {
		//converting datetime from format(yyyy-MM-dd'T'HH:mm:ss.SSS'Z) to time ago using pretty time library
		return Utils().convertStringToTimeAgo(modifiedDate)
	}
	fun isEmpty(): Boolean{
		return message.contains("not")
	}
}