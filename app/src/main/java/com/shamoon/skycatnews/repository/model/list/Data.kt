package com.shamoon.skycatnews.repository.model.list

import com.shamoon.skycatnews.utils.Utils
import java.io.Serializable

data class Data(
	val creationDate: String,
	val headline: String,
	val id: String,
	val modifiedDate: String?,
	val teaserImage: TeaserImage,
	val teaserText: String,
	val type: String,
	val url: String,
	val weblinkUrl: String
) : Serializable {
	fun getTimeAgo(
	): String {
		//converting datetime from format(yyyy-MM-dd'T'HH:mm:ss.SSS'Z) to time ago using pretty time library
		return Utils().convertStringToTimeAgo(modifiedDate ?: "")
	}
	fun isWebLink(): Boolean {
		return type.equals("weblink")
	}
	fun isStory(): Boolean {
		return type.equals("story")
	}
	fun isAdvert(): Boolean {
		return type.equals("advert")
	}
}