package com.shamoon.skycatnews.repository.model.list

import java.io.Serializable

data class TeaserImage(
	val _links: Links,
	val accessibilityText: String
) : Serializable