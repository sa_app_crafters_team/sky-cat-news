package com.shamoon.skycatnews.repository.model.list

import java.io.Serializable

data class Links(
	val url: Url
) : Serializable