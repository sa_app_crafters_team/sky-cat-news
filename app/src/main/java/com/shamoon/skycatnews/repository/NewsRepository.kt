package com.shamoon.skycatnews.repository

import android.content.Context
import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.shamoon.skycatnews.repository.api.Api
import com.shamoon.skycatnews.repository.api.Api.isAPIUrlAvailable
import com.shamoon.skycatnews.repository.model.list.NewsList
import com.shamoon.skycatnews.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRepository : BaseObservable() {
	val newsList: MutableLiveData<NewsList> by lazy {
		MutableLiveData<NewsList>()
	}

	fun fetchNews(context: Context) {
		if (isAPIUrlAvailable()) {
			val callback: Callback<NewsList> = object : Callback<NewsList> {
				override fun onResponse(call: Call<NewsList>, response: Response<NewsList>) {
					if (response.isSuccessful) {
						val body: NewsList? = response.body()
						newsList.value = body
						Log.d("news", newsList.toString())
					}
				}

				override fun onFailure(call: Call<NewsList>, t: Throwable) {
					Log.e("News List", t.message, t)
				}

			}

			Api.getApi().getNews().enqueue(callback)
		} else {
			getDataOffline(context)
		}
	}

	fun getDataOffline(context: Context){
		newsList.value = Utils().getNewsListFromFile(context)
	}

}