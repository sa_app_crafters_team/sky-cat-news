package com.shamoon.skycatnews

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.shamoon.skycatnews.vm.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class SkyCatNewsApp : MultiDexApplication() {

	override fun attachBaseContext(base: Context?) {
		super.attachBaseContext(base)
		MultiDex.install(this)
	}

	override fun onCreate() {
		super.onCreate()
		setupDependencyInjection()
	}

	//setup dependency injection
	private fun setupDependencyInjection() {
		startKoin {
			androidLogger(Level.DEBUG)
			androidContext(this@SkyCatNewsApp)
			modules(listOf(viewModelModule))
		}
	}
}