package com.shamoon.skycatnews.ui.base

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.shamoon.skycatnews.R

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {

	protected lateinit var mDataBinding: T

	@LayoutRes
	abstract fun provideLayoutResId(): Int

	abstract fun setupViewModel()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setupDataBinding()
		setupViewModel()


	}

	// Setup

	internal open fun setupDataBinding() {
		mDataBinding = DataBindingUtil.setContentView(this, provideLayoutResId())
	}
}