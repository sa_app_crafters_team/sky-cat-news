package com.shamoon.skycatnews.ui.activities.main

import android.content.Context
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.shamoon.skycatnews.R
import com.shamoon.skycatnews.constants.SingleLiveEvent
import com.shamoon.skycatnews.repository.NewsRepository
import com.shamoon.skycatnews.repository.model.list.Data
import com.shamoon.skycatnews.repository.model.list.NewsList
import com.shamoon.skycatnews.ui.adapters.NewsListAdapter
import com.shamoon.skycatnews.ui.base.BaseViewModel
import com.shamoon.skycatnews.utils.Utils

class MainViewModel(val context: Context) : BaseViewModel<MainViewModel.State>(State(null)) {
	private var adapter: NewsListAdapter = NewsListAdapter(R.layout.news, this)
	private var repository: NewsRepository = NewsRepository()

	var loading: ObservableInt = ObservableInt(View.GONE)
	var showEmpty: ObservableInt = ObservableInt(View.GONE)
	var showContent: ObservableInt = ObservableInt(View.VISIBLE)
	var selected: MutableLiveData<Data> = MutableLiveData()

	val eventStoryClicked = SingleLiveEvent<Data>()
	val eventWebLinkClicked = SingleLiveEvent<String>()
	val eventNoInternetConnection = SingleLiveEvent<Void>()
	val eventOnAdvertClick = SingleLiveEvent<String>()


	fun getAdapter(): NewsListAdapter {
		//return the adapter that we have to use in recyclerview
		return adapter
	}

	fun setupList() {
		loading.set(View.VISIBLE)
		showEmpty.set(View.GONE)
		if (Utils().isOnline(context)) {
			repository.fetchNews(context)
		} else {
			eventNoInternetConnection.call()
			loading.set(View.GONE)
			showEmpty.set(View.VISIBLE)
			repository.getDataOffline(context)
		}

	}

	fun getNewsList(): MutableLiveData<NewsList> {
		return repository.newsList
	}

	fun setNewsInAdapter(news: NewsList) {
		adapter.setNews(news.data as ArrayList<Data>)
		adapter.notifyDataSetChanged()
	}

	fun onItemClick(index: Int) {
		val db: Data? = getNewsAt(index)
		if (db != null) {
			if (db.isWebLink())
				eventWebLinkClicked.call(db.weblinkUrl)
			else if (db.isStory())
				eventStoryClicked.call(db)
			else if (db.isAdvert())
				eventOnAdvertClick.call(db.url)
		}
		selected.setValue(db)
	}

	private fun getNewsAt(index: Int): Data? {
		return if (repository.newsList
				.value != null &&
			repository.newsList.value?.data?.size!! > index
		) {
			repository.newsList.value?.data?.get(index)
		} else null
	}


	data class State(
		var newsList: NewsList?,
	)

}