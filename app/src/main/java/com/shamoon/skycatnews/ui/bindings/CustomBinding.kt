package com.shamoon.skycatnews.ui.bindings

import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shamoon.skycatnews.R
import com.shamoon.skycatnews.repository.model.list.Data
import com.shamoon.skycatnews.repository.model.list.NewsList
import com.shamoon.skycatnews.ui.activities.main.MainViewModel
import org.ocpsoft.prettytime.PrettyTime
import java.text.SimpleDateFormat
import java.util.*

object CustomBinding {
	@JvmStatic
	@BindingAdapter("imageUrl")
	fun loadImage(view: ImageView, url: String?) {
		Glide.with(view.context)
			.load(url)
			.placeholder(R.drawable.placeholder_skynews)
			.error(R.drawable.placeholder_skynews)
			.into(view)
	}

	@JvmStatic
	@BindingAdapter("imageUrl")
	fun loadImage(view: ImageView, data: Data?) {
		if (data?.isAdvert() == true){
			Glide.with(view.context)
				.load(R.drawable.advertise_here)
				.placeholder(R.drawable.advertise_here)
				.error(R.drawable.advertise_here)
				.into(view)
		}else{
			Glide.with(view.context)
				.load(data?.teaserImage?._links?.url?.href)
				.placeholder(R.drawable.placeholder_skynews)
				.error(R.drawable.placeholder_skynews)
				.into(view)
		}

	}

	@BindingAdapter("setAdapter")
	@JvmStatic
	fun bindRecyclerViewAdapter(
		recyclerView: RecyclerView,
		adapter: RecyclerView.Adapter<*>?
	) {
		recyclerView.setHasFixedSize(true)
		recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
		recyclerView.adapter = adapter
	}


}