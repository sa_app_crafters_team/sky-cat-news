package com.shamoon.skycatnews.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shamoon.skycatnews.BR
import com.shamoon.skycatnews.databinding.NewsBinding
import com.shamoon.skycatnews.repository.model.list.Data
import com.shamoon.skycatnews.ui.activities.main.MainViewModel

class NewsListAdapter(@LayoutRes private var layoutId: Int, private var viewModel: MainViewModel) :
	RecyclerView.Adapter<NewsListAdapter.GenericViewHolder>() {
	private var news: ArrayList<Data> = mutableListOf<Data>() as ArrayList<Data>

	fun setNews(categories: ArrayList<Data>) {
		this.news.clear()
		this.news.addAll(categories)
	}

	private fun getLayoutIdForPosition(position: Int): Int {
		return layoutId
	}

	override fun getItemViewType(position: Int): Int {
		return getLayoutIdForPosition(position)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
		val binding = DataBindingUtil.inflate<NewsBinding>(
			LayoutInflater.from(parent.context),
			viewType,
			parent,
			false
		)

		return GenericViewHolder(binding)
	}

	override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
		holder.bind(viewModel, position)
	}

	override fun getItemCount(): Int {
		return news.size
	}


	class GenericViewHolder(var binding: NewsBinding) : RecyclerView.ViewHolder(binding.root) {
		fun bind(viewModel: MainViewModel, position: Int?) {
			binding.setVariable(BR.position, position)
			binding.setVariable(BR.viewModel, viewModel)
			binding.executePendingBindings()
		}
	}
}