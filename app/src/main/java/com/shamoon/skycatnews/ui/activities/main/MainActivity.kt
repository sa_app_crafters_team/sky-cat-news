package com.shamoon.skycatnews.ui.activities.main

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.shamoon.skycatnews.R
import com.shamoon.skycatnews.databinding.MainActivityBinding
import com.shamoon.skycatnews.repository.model.list.NewsList
import com.shamoon.skycatnews.ui.activities.story.StoryActivity
import com.shamoon.skycatnews.ui.base.BaseActivity

class MainActivity : BaseActivity<MainActivityBinding>() {
	private val viewModel = MainViewModel(this@MainActivity)

	override fun provideLayoutResId(): Int {
		return R.layout.main_activity
	}

	//region Setup

	override fun setupDataBinding() {
		super.setupDataBinding()
		mDataBinding.viewModel = viewModel
		viewModel.setupList()
		setupData()
	}

	private fun setupData() {
		viewModel.loading.set(View.VISIBLE)
		val observer = Observer<NewsList> { newsList ->
			viewModel.loading.set(View.GONE)
			if (newsList == null) {
				viewModel.showEmpty.set(View.VISIBLE)
				viewModel.showContent.set(View.GONE)
			} else {
				viewModel.showEmpty.set(View.GONE)
				viewModel.showContent.set(View.VISIBLE)
				viewModel.state.value?.newsList = newsList
				viewModel.setNewsInAdapter(newsList)
			}
		}
		viewModel.getNewsList().observe(this, observer)
	}

	override fun setupViewModel() {
		viewModel.eventStoryClicked.observe(this) {
			//open story activity and send story id so that we can call to get that story
			startActivity(StoryActivity.launchIntent(this, it.id))
		}
		viewModel.eventWebLinkClicked.observe(this) {
			//open web link in browser with given url
			if (it.trim().isNotEmpty()) {
				var url = it
				if (!url.startsWith("http://") && !url.startsWith("https://"))
					url = "http://$url";
				val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
				startActivity(browserIntent)
			}
		}
		viewModel.eventNoInternetConnection.observe(this) {
			//show user no internet view/message
			val snack = Snackbar.make(
				mDataBinding.container,
				getString(R.string.no_internet),
				Snackbar.LENGTH_LONG
			)
			snack.show()
		}
		viewModel.eventOnAdvertClick.observe(this) {
			if (it.trim().isNotEmpty()) {
				var url = it
				if (!url.startsWith("http://") && !url.startsWith("https://"))
					url = "http://$url";
				val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
				startActivity(browserIntent)
			}
		}
	}
	//endregion
}