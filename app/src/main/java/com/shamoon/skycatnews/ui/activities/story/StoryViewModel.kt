package com.shamoon.skycatnews.ui.activities.story

import android.content.Context
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import com.shamoon.skycatnews.R
import com.shamoon.skycatnews.constants.SingleLiveEvent
import com.shamoon.skycatnews.repository.StoryRepository
import com.shamoon.skycatnews.repository.model.list.Data
import com.shamoon.skycatnews.repository.model.list.NewsList
import com.shamoon.skycatnews.repository.model.story.Content
import com.shamoon.skycatnews.repository.model.story.HeroImage
import com.shamoon.skycatnews.repository.model.story.Story
import com.shamoon.skycatnews.ui.adapters.NewsListAdapter
import com.shamoon.skycatnews.ui.adapters.StoryListAdapter
import com.shamoon.skycatnews.ui.base.BaseViewModel
import com.shamoon.skycatnews.utils.Utils
import java.io.Serializable

class StoryViewModel(id: String) :
	BaseViewModel<StoryViewModel.State>(State(id)) {
	private var adapter: StoryListAdapter = StoryListAdapter(R.layout.story, this)
	val repository = StoryRepository()
	var loading: ObservableInt = ObservableInt(View.GONE)
	var showEmpty: ObservableInt = ObservableInt(View.GONE)
	var showContent: ObservableInt = ObservableInt(View.VISIBLE)

	fun getStoryList(): MutableLiveData<Story> {
		return repository.story
	}
	fun getData(context: Context){
		if (Utils().isOnline(context))
			repository.fetchStory(context, state.value?.id ?: "1")
		else
			repository.fetchOfflineStory(state.value?.id ?: "1", context)
	}

	fun setStoryInAdapter(story: Story) {
		adapter.setContent(story.contents as ArrayList<Content>)
		adapter.notifyDataSetChanged()
	}

	fun getAdapter(): StoryListAdapter {
		//return the adapter that we have to use in recyclerview
		return adapter
	}

	data class State(
		val id: String,
		var headline: String = "",
		var heroImage: String = ""
	): Serializable {
		var story: Story = Story(listOf(),"","", HeroImage("",""),"","","Story does not exists")
	}
}