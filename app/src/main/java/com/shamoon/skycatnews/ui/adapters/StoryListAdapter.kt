package com.shamoon.skycatnews.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shamoon.skycatnews.BR
import com.shamoon.skycatnews.databinding.StoryBinding
import com.shamoon.skycatnews.repository.model.story.Content
import com.shamoon.skycatnews.ui.activities.story.StoryViewModel

class StoryListAdapter(
	@LayoutRes private var layoutId: Int,
	private var viewModel: StoryViewModel
) :
	RecyclerView.Adapter<StoryListAdapter.GenericViewHolder>() {
	private var content: ArrayList<Content> = mutableListOf<Content>() as ArrayList<Content>

	fun setContent(content: ArrayList<Content>) {
		this.content.clear()
		this.content.addAll(content)
	}

	private fun getLayoutIdForPosition(position: Int): Int {
		return layoutId
	}

	override fun getItemViewType(position: Int): Int {
		return getLayoutIdForPosition(position)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
		val binding = DataBindingUtil.inflate<StoryBinding>(
			LayoutInflater.from(parent.context),
			viewType,
			parent,
			false
		)

		return GenericViewHolder(binding)
	}

	override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
		holder.bind(viewModel, position)
	}

	override fun getItemCount(): Int {
		return content.size
	}


	class GenericViewHolder(var binding: StoryBinding) : RecyclerView.ViewHolder(binding.root) {
		fun bind(viewModel: StoryViewModel, position: Int) {
			binding.setVariable(BR.content, viewModel.state.value?.story?.contents?.get(position))
			binding.executePendingBindings()
		}
	}
}