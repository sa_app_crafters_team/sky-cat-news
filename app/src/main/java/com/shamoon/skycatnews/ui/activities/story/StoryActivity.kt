package com.shamoon.skycatnews.ui.activities.story

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import com.shamoon.skycatnews.R
import com.shamoon.skycatnews.constants.EXTRA_STORY_ID
import com.shamoon.skycatnews.databinding.ActivityStoryBinding
import com.shamoon.skycatnews.repository.model.story.Story
import com.shamoon.skycatnews.ui.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class StoryActivity : BaseActivity<ActivityStoryBinding>() {

	private val viewModel: StoryViewModel by viewModel {
		parametersOf(intent.getSerializableExtra(EXTRA_STORY_ID))
	}

	companion object {
		fun launchIntent(
			context: Context,
			storyID: String
		): Intent {
			val intent = Intent(context, StoryActivity::class.java)
			intent.putExtra(EXTRA_STORY_ID, storyID)
			return intent
		}
	}

	override fun provideLayoutResId(): Int {
		return R.layout.activity_story
	}

	override fun setupDataBinding() {
		super.setupDataBinding()
		mDataBinding.viewModel = viewModel
		viewModel.getData(this)
		viewModel.loading.set(View.VISIBLE)
		val observer = Observer<Story> { story ->
			viewModel.loading.set(View.GONE)
			if (story.isEmpty()) {
				viewModel.showEmpty.set(View.VISIBLE)
				viewModel.showContent.set(View.GONE)
			} else {
				viewModel.showEmpty.set(View.GONE)
				viewModel.showContent.set(View.VISIBLE)
				viewModel.state.value?.story = story
				viewModel.state.value?.headline = story.headline
				viewModel.state.value?.heroImage = story.heroImage.imageUrl

				viewModel.setStoryInAdapter(story)
			}
		}
		viewModel.getStoryList().observe(this, observer)
	}

	override fun setupViewModel() {
		viewModel.state.observe(this) { state ->
			mDataBinding.state = state
		}
	}


}