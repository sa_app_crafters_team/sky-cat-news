package com.shamoon.skycatnews.utils

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.shamoon.skycatnews.R
import com.shamoon.skycatnews.repository.model.list.NewsList
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class UtilsTest{
	lateinit var instrumentationContext: Context
	@After
	fun after(){
		stopKoin()
	}
	@Before
	fun setup() {
		stopKoin()
		instrumentationContext = InstrumentationRegistry.getInstrumentation().context
	}

	@Test
	fun getNewsListFromFileTest(){
		//test for data list should have values
		assertTrue((Utils().getNewsListFromFile(instrumentationContext).data?.size ?: 0 > 0))
		assertEquals(Utils().getNewsListFromFile(instrumentationContext).title, instrumentationContext.getString(R.string.app_name))
	}

	@Test
	fun getStoryFromFileTest(){
		//test for data list should have values
		assertTrue((Utils().getStoryFromFile(instrumentationContext).contents.isNotEmpty()))
		assertTrue(Utils().getStoryFromFile(instrumentationContext).getTimeAgo().isNotEmpty())
	}

	@Test
	fun timeAgoTest(){
		assertTrue(Utils().convertStringToTimeAgo("2020-11-18T00:00:00Z").isNotEmpty())
		assertEquals(Utils().convertStringToTimeAgo("2020-11-18T00:00:00Z"), "5 months ago")
	}
}