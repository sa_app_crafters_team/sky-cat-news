package com.shamoon.skycatnews.repository.api

import org.junit.Assert
import org.junit.Test

class ApiTest {
	@Test
	fun newsApiTest() {
		// call the api
		val api = Api.getApi()
		Assert.assertNotNull(api)
		val response = api.getNews().execute()
		// verify the response is OK
		Assert.assertTrue(response.code() == 200)
	}

	@Test
	fun storyApiTest() {
		// call the api
		val api = Api.getApi()
		Assert.assertNotNull(api)
		val response = api.getStory("1").execute()
		// verify the response is OK
		Assert.assertTrue(response.code() == 200)
	}
}