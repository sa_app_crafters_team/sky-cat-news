package com.shamoon.skycatnews.repository.model.story

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.shamoon.skycatnews.utils.Utils
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class StoryTest{
	lateinit var instrumentationContext: Context

	@After
	fun after(){
		stopKoin()
	}
	@Before
	fun setup() {
		stopKoin()
		instrumentationContext = InstrumentationRegistry.getInstrumentation().context
	}
	companion object {
		fun story(context: Context): Story {
			return Utils().getStoryFromFile(context)
		}
	}

	@Test
	fun test_getHeadline() {
		val story = story(instrumentationContext)
		Assert.assertNotNull(story.headline)
		Assert.assertNotNull(story.getTimeAgo())
	}
}