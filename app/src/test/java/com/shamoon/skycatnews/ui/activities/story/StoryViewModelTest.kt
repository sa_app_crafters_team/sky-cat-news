package com.shamoon.skycatnews.ui.activities.story

import android.content.Context
import android.view.View
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class StoryViewModelTest {
	lateinit var instrumentationContext: Context
	lateinit var viewModel: StoryViewModel

	@Before
	fun setUp() {
		stopKoin()
		instrumentationContext = InstrumentationRegistry.getInstrumentation().context
		viewModel = StoryViewModel("2")
		viewModel.getData(instrumentationContext)
	}

	@After
	fun tearDown() {
		stopKoin()
	}

	@Test
	fun shouldNotHaveDataForWrongID() {
		assertTrue(viewModel.state.value?.story?.isEmpty() ?: false)
	}
}