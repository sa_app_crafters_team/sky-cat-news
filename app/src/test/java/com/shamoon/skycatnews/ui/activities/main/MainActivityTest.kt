package com.shamoon.skycatnews.ui.activities.main

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.shamoon.skycatnews.R
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner


@RunWith(RobolectricTestRunner::class)
class MainActivityTest {
	lateinit var activity: MainActivity


	@Before
	fun setup() {
		stopKoin()
		activity = Robolectric.buildActivity(MainActivity::class.java)
			.create()
			.resume()
			.get()
	}

	@After
	fun after(){
		stopKoin()
	}


	@Test
	@Throws(java.lang.Exception::class)
	fun shouldHaveDefaultMargin() {
		val imageView = activity.findViewById<View>(R.id.imageView) as ImageView
		val bottomMargin = (imageView.layoutParams as ConstraintLayout.LayoutParams).bottomMargin
		assertEquals(5, bottomMargin)
		val topMargin = (imageView.layoutParams as ConstraintLayout.LayoutParams).topMargin
		assertEquals(0, topMargin)
		val rightMargin = (imageView.layoutParams as ConstraintLayout.LayoutParams).rightMargin
		assertEquals(0, rightMargin)
		val leftMargin = (imageView.layoutParams as ConstraintLayout.LayoutParams).leftMargin
		assertEquals(0, leftMargin)
	}

}